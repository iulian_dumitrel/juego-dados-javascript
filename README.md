# Juego dados JavaScript

Un juego muy casual, y simple, realizado mediante la manipulación de DOM

## Instrucciones

Para jugar se puede hacer mediante los botones ("tira los dados" y "juega otra vez") que a su vez también son accionados con las teclas "Espacio" y "R".

Para ganar hay que sacar el mismo numero en ambos dados ( de 1 a 6  ) una vez que coincidan, se muestran los intentos.
