"use strict";

// Declaración de variables con elementos que empiezan acciones
const againBtn = document.querySelector(".again");
const rollBtn = document.querySelector(".rollbtn");

// Decl variable con elemento HTML
const number1 = document.querySelector(".number-1");
const number2 = document.querySelector(".number-2");
let body = document.querySelector("body");
let pagetitle = document.querySelector("#pagetitle");
// Mensajes y cuadro html con los intentos
let msg1 = document.querySelector(".message");
let msg2 = document.querySelector(".message-2");
let intentosDisplay = document.querySelector(".intentos");

// Contadores que luego cambian en funcion del flujo del programa
let intentos = 0;
let victoria = false;

function jugar(){
  if (victoria == false) {
    let dice1 = Math.trunc(Math.random() * 6) + 1;
    let dice2 = Math.trunc(Math.random() * 6) + 1;
    // Muestro el val de los dados en el html
    number1.textContent = dice1;
    number2.textContent = dice2;

    // Aumento contador intentos y muestro en HTML
    intentos++;
    intentosDisplay.textContent = intentos;

    // Si los valores de los dados son iguales, GANAMOS
    if (dice1 === dice2) {
      number1.textContent = dice1;
      number2.textContent = dice2;
      pagetitle.textContent = "Has ganado!";
      victoria = true;
      body.style.backgroundColor = "green";
      intentosDisplay.textContent = `Lo has hecho en ${intentos} ${
        intentos === 1 ? "intento" : "intentos"
      }`;
    }
  } else { //  Si victoria==true, hemos ganado!
    msg2.textContent = "Vuelve a jugar!(R)";
  }
}

function resetearJuego(){
  body.style.backgroundColor = "#222";
  intentos = 0;
  intentosDisplay.textContent = intentos;
  number1.textContent = "?";
  msg2.textContent = "";
  number2.textContent = "?";
  pagetitle.textContent = "Consigue el mismo número en ambos dados para ganar";
  victoria = false;
}

// Cúando se clica en "Roll", tira los dos dados.
rollBtn.addEventListener("click", jugar);

// Event Listener de la tecla "espacio", para rollear con ella.
document.addEventListener("keyup", function(e){
  // si la propiedad "key" es igual a espacio, se ejecuta funcion jugar. -> si se presiona espacio > jugar()
  if(e.key === " ") jugar();
});

// Funcion para volver a jugar, devuelve los parametros a un estado inicial.
againBtn.addEventListener("click", resetearJuego);
// Event listener de la tecla "r", para resetear juego
document.addEventListener("keyup", function(e){
  if(e.key === "r" || e.key === "R") resetearJuego();
});
